<?php require 'databaseCheck.php';


    $userName = $_GET['sn'];
    $selectedGame = $_GET['g'];
    $SelectedConsole = $_GET['c'];

    //OPENING PROCEDURAL CONNECTION TO MYSQL DATABASE ON LOCAL HOST USING VARIABLES GIVEN BY //SCRIPTS/DATABASECHECK.PHP
    $conn = new mysqli($SERVER_NAME,$USER_NAME,$PASSWORD,$DATABASE,$PORT);

    if ($conn -> connect_errno) {
        die('Connection error, please try again soon.: ' . $conn->connect_error);
    } else {

        //echo "Username: " . $userName . " Selected Game: " . $selectedGame . " Selected Console: " . $SelectedConsole;

        $sql = "Select * from gamers.requests WHERE RequestPlatform = '" . $SelectedConsole ."' and RequestGame = '" . $selectedGame ."';";
        //$sql2 = "Select UserEmail,usersId from gamers.users where UserScreenName = '" . $userName ."';";



        $foundResults = $conn->query($sql);

        if ($foundResults === false) {
            die("0");
        } else {

            //GATHERING THE NUMBER OF ROWS RETURNED
            $rows_returned = $foundResults->num_rows;

            $foundResults->data_seek(0);


            $rows = array();

            while($r = mysqli_fetch_assoc($foundResults)) {

                $time = strtotime($r{"RequestTime"});
                $timeNow = strtotime(date('Y-m-d H:i:s'));
                $timeDiff = $timeNow - $time;


                if($timeDiff < 3600){
                    $rows[] = $r;
                }
            }
            print json_encode($rows);


        }

    }