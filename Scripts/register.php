<?php require"databaseCheck.php";

    $userFirstName    = $_GET['fn'];
    $userLastName    = $_GET['ln'];
    $userScreenName   = $_GET['sn'];
    $userEmail       = $_GET['email'];
    $userHashedPassword = password_hash($_GET['pw'],PASSWORD_DEFAULT);


    if ($userFirstName && $userLastName && $userScreenName && $userEmail && $userHashedPassword != null){

        $conn = new mysqli($SERVER_NAME,$USER_NAME,$PASSWORD,$DATABASE,$PORT);

        $safeFirstName = mysqli_real_escape_string($conn,$userFirstName);
        $safeLastName = mysqli_real_escape_string($conn, $userLastName);
        $safeUserName = mysqli_real_escape_string($conn, $userScreenName);
        $safeEmail = mysqli_real_escape_string($conn,$userEmail);

        if ($conn -> connect_errno) {
            die("Error: " . mysqli_connect_error());
        } else {

            $sql = "Select 1 from users WHERE UserScreenName = '" . $safeUserName . "';";

            $userFoundResults = $conn->query($sql);

            if ($userFoundResults === false) {
                echo "Error: no results ";
            } else {

                $rows_returned = $userFoundResults->num_rows;

                if ($rows_returned === 1) {

                   /*
                    * Have to send error code of user allready found in data base.
                    *
                    */

                    echo "User was found in database.";
                } else {
                    //Checking email
                    $sql = "Select 1 from users WHERE UserEmail = '" . $safeEmail . "';";

                    $userFoundResults = $conn->query($sql);

                    if ($userFoundResults === false) {
                        echo "Error: no results ";
                    } else {
                        $rows_returned = $userFoundResults->num_rows;
                        if ($rows_returned === 1) {
                            echo "Email is all ready in database";
                        } else{

                            //MySqli Insert Query

                            $sql = "INSERT INTO users(usersId,UserFirstName,UserLastName,UserScreenName,UserEmail,UserRegistrationDate,UserHashedPassword) VALUES(null,'" . $safeFirstName . "','" . $safeLastName . "','" . $safeUserName . "','" . $safeEmail . "',CURRENT_TIMESTAMP,'" . $userHashedPassword . "');";

                            $insert_row = $conn->query($sql);


                            if($insert_row){
                               print 'Success! URL ENCODING DONE AND IT HAS BEEN UPDATED : insert id -> ' .$conn->insert_id .'<br />';
                            }else{
                              die('Error : ' . $conn -> error);
                            }
                        }
                    }
                }
            }
        }
    } else {
        //VARIABLE WAS NOT SET IN THE URL ENCODING.
        echo '0.1 -> Variables not set in url encoding';
    }


