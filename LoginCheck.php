<?php require 'Scripts/databaseCheck.php';


    //REQUIRED IN API AS KEY VALUE PAIRS, such as  xxx.php?id=xxxxx&pw=xxxxx
    $userName = $_GET['id'];
    $passwordDefault = $_GET['pw'];

    //HASHING PASSWORD TO STORE INTO DATABASE
    $passwordHashed = password_hash($passwordDefault,PASSWORD_DEFAULT);

    //OPENING PROCEDURAL CONNECTION TO MYSQL DATABASE ON LOCAL HOST USING VARIABLES GIVEN BY //SCRIPTS/DATABASECHECK.PHP
    $conn = new mysqli($SERVER_NAME,$USER_NAME,$PASSWORD,$DATABASE,$PORT);

    //CHECKING TO SEE IF USERNAME AND PASSWORD ARE NOT NULL
    if($userName && $passwordDefault != null){

        //CHECKING CONNECTION STATUS
        if ($conn -> connect_errno) {
            die('Connection error, please try again soon.: ' . $conn->connect_error);
        } else {

            // THIS SQL STATEMENT FETCHES THE USERSCREENNAME AND USERHASHEDPASSWORD FROM USERS TO USE ON PASSWORD CHECK
            $sql = "Select UserScreenName,UserHashedPassword from users WHERE UserScreenName = '" . $userName . "';";

            //GATHERING THE SQL STATEMENT DATA AND PLACING IT INTO USERFOUNDRESULTS
            $userFoundResults = $conn->query($sql);

            //CHECKING IF USERFOUDNRESULTS DOES NOT === NULL
            if ($userFoundResults === false) {
                die("0");
            } else {

                //GATHERING THE NUMBER OF ROWS RETURNED
                $rows_returned = $userFoundResults->num_rows;

                //IF ROWS RETURNED IS 1
                if ($rows_returned === 1) {

                    $userFoundResults ->data_seek(0);

                    while ($row = $userFoundResults->fetch_assoc()){

                        //GATHERING HASHED PASSWORD FROM DATABASE TO CHECK AGAINST
                        $userHashedPassword = $row['UserHashedPassword'] ;

                        //CHECKING PASSWORD AGAINST HASH IN DATABASE
                        if (password_verify($passwordDefault,$userHashedPassword)) {
                            //USERNAME AND PASSWORD WERE SUCCESSFULLY CHECK AGAINST DATABASE SENDING CODE 1 TO LOGIN
                            echo "1";
                        } else {
                            //INVALID PASSWORD
                            echo "0.4";
                        }
                    }
                } else {
                    //USER WAS NOT FOUND IN DB
                    echo "0.3";
                }
            }

        }
    } else {
        //USER NAME OR PASSWORD WAS NOT ENTERED
        if ($userName == null){
           //NO USER NAME WAS ENTERED
            echo "0.1";
        } else if ($passwordDefault == null){
            //NO PASSWORD WAS ENTERED
            echo "0.2";
        }
    }

    $conn -> close();
?>